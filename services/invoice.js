
var DbUtil = require('./util/dbUtil');
var sql = require('./config/sql.json');
var commonUtils = require('./util/common');
var mysqlDateTimeFormat = 'YYYY-MM-DD HH:mm:ss';

function calculateRemainingProductCount(companyId,source,invoiceType,productData,invoiceId, callback ) {
    var queryToGetProductQTY;
    var connection = new DbUtil();
    var preparedData = [];
    if(source == 'INSERT') {
        queryToGetProductQTY = sql.product.getProductQty;
        preparedData = [ companyId ];
    } else if(source == 'UPDATE') {
        queryToGetProductQTY = sql.product.updateAvailQtyForUpdate;
        preparedData = [ invoiceId, companyId ];
    }
    var prodIds = '( '
        var invoiceProductsMap = new Map();
        for(var count = 0; count < productData.length; count++) {
            prodIds += productData[count].product_id + ',';
            invoiceProductsMap.set(productData[count].product_id, productData[count]);      
        }
        prodIds = prodIds.slice(0, -1) + ' )'; 
        queryToGetProductQTY = queryToGetProductQTY.replace('%PRODUCTIDS%', prodIds);
           console.log(queryToGetProductQTY);
    connection.fetchSQLWithData(queryToGetProductQTY,preparedData, function(err, rows) {
        if(err) throw err;
        var caseQuery = ' ';
        for (var prodCount = 0; prodCount < rows.length; prodCount++){
            var prodQty =  (invoiceProductsMap.get(rows[prodCount].product_id) == null || undefined ) ? 0 : invoiceProductsMap.get(rows[prodCount].product_id).quantity;
            
            var updatedAvailableQty = 0;
            if(invoiceType == 'sale') {
                updatedAvailableQty = parseFloat(rows[prodCount].available_quantity) - parseFloat(prodQty);
            }
            else if(invoiceType == 'purchase') {
                updatedAvailableQty = parseFloat(rows[prodCount].available_quantity) + parseFloat(prodQty);
            }
            caseQuery += " WHEN product_id = " + rows[prodCount].product_id + " THEN " + updatedAvailableQty; 
        }
       var updateProductQtyQuery = sql.product.updateAvailQty.replace('%AVAILQTYCASEQUERY%',caseQuery).replace('%PRODUCTIDS%', prodIds);
       console.log(updateProductQtyQuery); 
       connection.executeSQL(updateProductQtyQuery,[companyId], function(err, result){
         if(err) throw err;
         return callback(err, result);
     });
   });
}

module.exports.addProdtoInvoce = function(companyData, productData, cb) {
    var connection = new DbUtil();
    const companyId = commonUtils.getFromLocal('companyId');
    var userId = commonUtils.getFromLocal('userId');
    var invoiceData = {
        invoice_type : companyData.invoiceType,
        company_id_fk : companyId,
        client_id_fk : companyData.client_id,
        purchased_date : companyData.purchased_date,
        issued_date : companyData.issued_date,
        active_flag : 'Y',
        shipping_charge : companyData.shippingCharge,
        shipping_tax : companyData.shippingTax,
        products_amount : companyData.totalAmtForAllProduct,
        paid_by : companyData.cashPaidBy,
        paid_amount : companyData.paidAmount,
        products_discount_amount : companyData.totalDiscountForAllProduct,
        products_gst_amount :companyData.totalGSTAmountForAllProduct,
        created_date : commonUtils.formatDate(companyData.createdDate, mysqlDateTimeFormat),
        updated_date : commonUtils.formatDate(companyData.createdDate, mysqlDateTimeFormat),
        created_by : userId,
        updated_by : userId
    };
    
    var queryToInsertInvoice = sql.invoice.queryInsertInvoice;
    connection.executeSQL(queryToInsertInvoice,invoiceData, function(err, result){
        if(err) throw err;
        var invoiceId = result.lastID;
        console.log("Hello " + JSON.stringify(result));
        var exceuteInvoiceListData = [];    
        
        for(var pCount = 0; pCount < productData.length; pCount++) {
            let insertId = pCount + 1; 
            var array = [
                insertId, // auto Increment id
                invoiceId, // 
                productData[pCount].product_id,
                productData[pCount].product_name,
                productData[pCount].description,
                productData[pCount].uom,
                productData[pCount].quantity,
                productData[pCount].discount,
                productData[pCount].discountAmount,
                productData[pCount].gst,
                productData[pCount].gstAmount,
                productData[pCount].price,  // single product price
                productData[pCount].totalAmount, // qty * prod price
                productData[pCount].priceAfterDiscountForProduct, // adding gst and deducting discount
                productData[pCount].pActive,
                companyData.userId,
                companyData.userId,
                commonUtils.formatDate(companyData.createdDate, mysqlDateTimeFormat),
                commonUtils.formatDate(companyData.createdDate, mysqlDateTimeFormat)
            ];
            exceuteInvoiceListData.push(array);
        }

        calculateRemainingProductCount(companyId,'INSERT',companyData.invoiceType,productData,'',function(err,resData){
        console.log(exceuteInvoiceListData);
       var queryExceInvloiceValueLst = sql.invoice.queryProductInsert;
       connection.executeSQL(queryExceInvloiceValueLst,exceuteInvoiceListData, function(err, result){
        if(err) throw err;
            console.log(JSON.stringify(result));
            
              var resp = {
                 success : true,
                 message : 'update Success',
                 invoiceId : invoiceId
              }
              return cb(err, resp);
        });
     });
   });
};


module.exports.updateProdtoInvoce = function(invoiceId,companyData, productData, cb) {
    var connection = new DbUtil();
    var currentDate = new Date();
    const companyId = commonUtils.getFromLocal('companyId');
    console.log('invoice tpe is ' + JSON.stringify(companyData));
    var userId = commonUtils.getFromLocal('userId');
    var invoiceData = {
        client_id_fk : companyData.client_id,
        purchased_date : companyData.purchased_date,
        issued_date : companyData.issued_date,
        active_flag : 'Y',
        shipping_charge : companyData.shippingCharge,
        shipping_tax : companyData.shippingTax,
        products_amount : companyData.totalAmtForAllProduct,
        products_discount_amount : companyData.totalDiscountForAllProduct,
        products_gst_amount :companyData.totalGSTAmountForAllProduct,
        paid_by : companyData.cashPaidBy,
        paid_amount : companyData.paidAmount,
        updated_date : commonUtils.formatDate(currentDate, mysqlDateTimeFormat),
        updated_by : userId
    };
    
    
    var queryToUpdateInvoice = sql.invoice.queryUpdateInvoice;
    connection.executeSQLWithValues(queryToUpdateInvoice,invoiceData,[invoiceId,companyId], function(err, result){
        if(err) throw err;
        

        console.log("Hello " + JSON.stringify(result));
        var exceuteInvoiceListData = [];    
        for(var pCount = 0; pCount < productData.length; pCount++) {
            let insertId = pCount + 1;
            var array = [
                insertId, // auto Increment id
                invoiceId, // 
                productData[pCount].product_id,
                productData[pCount].product_name,
                productData[pCount].description,
                productData[pCount].uom,
                productData[pCount].quantity,
                productData[pCount].discount,
                productData[pCount].discountAmount,
                parseFloat(productData[pCount].gst).toFixed(2),
                productData[pCount].gstAmount,
                productData[pCount].price,  // single product price
                productData[pCount].totalAmount, // qty * prod price
                productData[pCount].priceAfterDiscountForProduct, // adding gst and deducting discount
                productData[pCount].pActive,
                companyData.userId, // needs to change the created By
                companyData.userId, 
                commonUtils.formatDate(companyData.createdDate, mysqlDateTimeFormat),
                commonUtils.formatDate(currentDate, mysqlDateTimeFormat)
            ];
            exceuteInvoiceListData.push(array);
        }
        calculateRemainingProductCount(companyId,'UPDATE',companyData.invoiceType,productData,invoiceId,function(err,resData){

       var queryExceInvloiceValueLst = sql.invoice.queryProductUpdate;
       connection.executeSQL(queryExceInvloiceValueLst,exceuteInvoiceListData, function(err, result){
        if(err) throw err;
            console.log(JSON.stringify(result));
            
       
            var resp = {
                success : true,
                message : 'update Success',
                invoiceId : invoiceId
            }
            return cb(err, resp);
       });
     });
   });
};

module.exports.getAllInvoice = function(cb) {
    var resp;
    try {
       var connection = new DbUtil();
       var queryFetchAllInvoices = sql.invoice.queryFetchInvoices;
       const companyId = commonUtils.getFromLocal('companyId'); 
       connection.fetchSQLWithData(queryFetchAllInvoices,[companyId], function(err, rows) {
        if(err) throw err;
      
           if(rows.length == 0) {
               resp = {
                   success : false,
                   message : 'No Results'
               };
           }
           else {
               resp = {
                 success : true,
                 message : 'fetch Data Successful',
                 data : rows
             };
           }
           return cb(null, resp); 
       }); 
  } catch(ex) {
     resp = {
       success : false,
       message : 'Exeption : ' + ex
     };
     console.log(JSON.stringify(resp));    
     return cb(null, resp); 
   }
};

module.exports.getInvoiceData = function(invoiceId, cb) {
    var resp;
    try {
        var connection = new DbUtil();
        var resp;
        var queryFetchAllInvoices = sql.invoice.queryFetchInvoiceDataById;
        
        const companyId = commonUtils.getFromLocal('companyId'); 
        connection.fetchSQLWithData(queryFetchAllInvoices,[invoiceId,companyId], function(err, rows) {
            if(err) throw err;
            if(rows.length == 0) {
                resp = {
                    success : false,
                    message : 'No Results'
                };
            }
            else {
                resp = {
                  success : true,
                  message : 'fetch Data Successful',
                  data : rows
              };
            }
            return cb(null, resp); 
        });     
    } catch(ex) {
     resp = {
       success : false,
       message : 'Exeption : ' + ex
     };
     console.log(JSON.stringify(resp));    
     return cb(null, resp); 
   }
};