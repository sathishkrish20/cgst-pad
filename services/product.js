var DbUtil = require('./util/dbUtil');
var sql = require('./config/sql.json');
var commonUtils = require('./util/common');

var mysqlDateTimeFormat = 'YYYY-MM-DD HH:mm:ss';


module.exports.getAllProducts = function(cb) {
    try {
         var connection = new DbUtil();
         
         var resp;
         var queryFetchAllProducts = sql.product.queryFetchProducts;
         const companyId = commonUtils.getFromLocal('companyId');
        
         connection.fetchSQLWithData(queryFetchAllProducts,companyId, function(err, rows) {
            if(err) throw err;
            console.log(rows);
             if(rows.length == 0) {
                 resp = {
                     success : false,
                     message : 'No Results,Click to Add the Product',
                     count : rows.length
                 };
             }
             else {
                 resp = {
                   success : true,
                   message : 'fetch Data Successful',
                   data : rows,
                   count : rows.length
               };
             }
             return cb(null, resp); 
         }); 
    } catch(ex) {
       resp = {
         success : false,
         message : 'Exeption : ' + ex
       };
       console.log(JSON.stringify(resp));    
       return cb(null, resp); 
     }
  };

  module.exports.insertProduct = function(productData, cb) {
    var response;
    try { 
    var connection = new DbUtil();
    var queryToInsertProduct = sql.product.queryInsertProduct;
    
    const companyId = commonUtils.getFromLocal('companyId');
    productData.company_id_fk = companyId;
    productData.active_flag = 'Y';
    productData = commonUtils.formRequestData(productData,'insert');
    
    console.log(productData);

    connection.executeSQL(queryToInsertProduct,productData, function(err, result){
     if(err) throw err;
        response = {
            success : true,
            message : 'Insert Success',
            data : result
        };
        console.log(response);
        return cb(err, response);
    });
 } catch(ex) {
      response = {
        success : false,
        message : 'Exeption : ' + ex
      };
      console.log(JSON.stringify(response));    
      return cb(null, response);    
  }
};

module.exports.updateProduct = function(productId,productData, cb) {
    var response;
    try { 
    var connection = new DbUtil();
    var queryToUpdateProduct = sql.product.queryUpdateProduct;
    const companyId = commonUtils.getFromLocal('companyId');

    updateData = commonUtils.formRequestData(JSON.parse(productData),'update');
    
    connection.executeSQLWithValues(queryToUpdateProduct,updateData,[ companyId, productId ], function(err, result){
     if(err) throw err;
        console.log(result);
        response = {
            success : true,
            message : 'update Success'
        };
        console.log(response);
        return cb(err, response);
    });
 } catch(ex) {
      response = {
        success : false,
        message : 'Exeption : ' + ex
      };
      console.log(JSON.stringify(response));    
      return cb(null, response);    
  }
}

module.exports.deleteProduct = function(productId, cb) {
    var response;
    try { 
    var connection = new DbUtil();
    var currentDate = new Date();
    var queryToDeleteProduct = sql.product.queryDeleteProductById;
    
    
    const companyId = commonUtils.getFromLocal('companyId');
    var updated_date = commonUtils.formatDate(currentDate,mysqlDateTimeFormat);
    var updated_by = commonUtils.getFromLocal('userId');
    
    var executeData = [];
    executeData.push([
        'N',
        updated_by,
        updated_date,
        companyId,
        productId
    ]);

    connection.executeSQL(queryToDeleteProduct,executeData, function(err, result){
     if(err) throw err;
        console.log(result);
        response = {
            success : true,
            message : 'Delete Success',
            data : result
        };
        console.log(response);
        return cb(err, response);
    });
 } catch(ex) {
      response = {
        success : false,
        message : 'Exeption : ' + ex
      };
      console.log(JSON.stringify(response));    
      return cb(null, response);    
  }
}
