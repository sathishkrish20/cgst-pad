var DbUtil = require('./util/dbUtil');
var sql = require('./config/sql.json');
var commonUtils = require('./util/common');
var mysqlDateTimeFormat = 'YYYY-MM-DD HH:mm:ss';
var request = require('request');
const Store = require('electron-store');

module.exports.getCompany = function(cb) {
    var response;
    try {
        var connection = new DbUtil();
        const store = new Store();
        const companyId = store.get('companyId');
        console.log(companyId);
       var queryFetchAllInvoices = sql.company.queryFetchCompany;
       connection.fetchSQLWithData(queryFetchAllInvoices, companyId, function(err, rows) {
            response = {
                 success : true,
                 message : 'fetch Data Successful',
                 data : rows
             };
             return cb(null, response); 
       });         
    } catch(ex) {
        response = {
            success : false,
            message : 'Exeption : ' + ex
          };
          console.log(JSON.stringify(response));    
          return cb(null, response); 
    }
};

module.exports.updateCompany = function(companyData, cb) {
    var response;
    try { 
    var connection = new DbUtil();
    var queryToUpdateCompany = sql.company.queryUpdateCompany;
    
    const store = new Store();
    const companyId = store.get('companyId');
    connection.executeSQL(queryToUpdateCompany.replace('%COMPANYID%',companyId),companyData, function(err, result){
     if(err) throw err;
        store.set('companyId', companyId);
        console.log(result);
        response = {
            success : true,
            message : 'update Success'
        };
        console.log(response);
        return cb(err, response);
    });
 } catch(ex) {
      response = {
        success : false,
        message : 'Exeption : ' + ex
      };
      console.log(JSON.stringify(response));    
      return cb(null, response);    
  }
}

module.exports.insertCompany = function(companyData, cb) {
    var response;
    try{ 
      var connection = new DbUtil();
      const store = new Store();
      console.log(companyData);
      var options = {
        url: 'http://ayyavaikundaswamy.com/C-GST/signup.php',
        form : companyData
      };

      request.post(options, function (error, response, body) {
        if(error) throw error;
        if(response.statusCode !== 200) {
            response = {
                message : 'Something Went Wrong Try Again Later,Contact Admin',
                success : 'false',
                statusCode : response.statusCode
            }
            console.log(response);
            return cb(null, response);    
        }
        var result =  JSON.parse(body);
        var companyId = result.clientId;
        var queryToInsertCompany = sql.company.queryInsertCompany;
        companyData.id = companyId;
    
       connection.executeSQL(queryToInsertCompany,companyData, function(err, result){
        if(err) throw err;
        console.log(result);
        store.set('companyId', companyId);
          response = {
            success : true,
            message : 'Insert Success',
            invoiceId : result.lastID
        }
        return cb(err, response);
    });
  });
 } catch(ex) {
      response = {
        success : false,
        message : 'Exeption : ' + ex
      };
      console.log(JSON.stringify(response));    
      return cb(null, response);    
  }
}