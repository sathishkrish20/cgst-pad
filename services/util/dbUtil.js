var sqlite3 = require('sqlite3').verbose();
//var async= require('async');
function DbUtil() {};

DbUtil.prototype.executeSQL =function(query,data, cb ) {
    var db = new sqlite3.Database('db.db');
   
    db.serialize(function() {
        
        if(data.constructor == Object){
            var newQueryData = makeQueryDataForObject(query, data,[]);
            query = newQueryData.query;
            data = newQueryData.data;
        }
        
        var stmt = db.prepare(query);
        for (var i = 0; i < data.length; i++) {
            
          stmt.run(data[i], function(err){
               if(err) throw err;
          });
        }
        stmt.finalize(function(err){
            return cb(err, this);
        });
      });
    
      db.close(); 
}

DbUtil.prototype.executeSQLWithValues =function(query,data,whereData, cb ) {
  var db = new sqlite3.Database('db.db');
 
  db.serialize(function() {
      
      if(data.constructor == Object){
          var newQueryData = makeQueryDataForObject(query, data,whereData);
          query = newQueryData.query;
          data = newQueryData.data;
      }
      console.log(query);
      console.log(data);
      console.log(data.constructor);
      var stmt = db.prepare(query);
      for (var i = 0; i < data.length; i++) {
          stmt.run(data[i], function(err){
             if(err) throw err;
        });
      }
      stmt.finalize(function(err){
          return cb(err, this);
      });
    });
  
    db.close(); 
}

DbUtil.prototype.fetchSQL = function(query, cb ) {
    var db = new sqlite3.Database('db.db');
        db.all(query,function(err, rows) {
          if(err) throw err;
            return cb(null,rows);
          });
        db.close();    
}
DbUtil.prototype.fetchSQLWithData = function(query,data, cb ) {
  var db = new sqlite3.Database('db.db');
      db.all(query,data,function(err, rows) {
        if(err) throw err;
          return cb(null,rows);
        });
      db.close();    
}


function makeQueryDataForObject(query, data,whereData) {
  
  var queryNew = query.trim().toLowerCase();
  var dataArray = [];
  var columnNames = '';
  var preparedStmtQ ='';
  if(queryNew.substring(0, 6) == 'insert') {
    
     
    
    for(var i = 0; i < Object.keys(data).length; i++) {
        columnNames += Object.keys(data)[i] +',';
        preparedStmtQ += '?,';
        dataArray.push(data[Object.keys(data)[i]]);
     }
     for(var j=0; j < whereData.length; j++) {
         dataArray.push(whereData[j]);
      }
     columnNames = '(' + columnNames.slice(0, -1) + ')'; // replacing the last Char
   preparedStmtQ = '(' + preparedStmtQ.slice(0, -1) + ')';
     var preparedQuery = columnNames + ' VALUES ' + preparedStmtQ; 
     queryNew = queryNew.replace('set ?', preparedQuery);
  } else if(queryNew.substring(0, 6) == 'update') {
       
       for(var i = 0; i < Object.keys(data).length; i++) {
         columnNames += Object.keys(data)[i] +' = ' + '? ,';
         dataArray.push((data[Object.keys(data)[i]]));
       }
       for(var j=0; j < whereData.length; j++) {
          dataArray.push(whereData[j]);
       }
       columnNames = columnNames.slice(0, -1); //slicing the last Char, that is comma
       queryNew = queryNew.replace('?', columnNames);
    }  

    
     return { query : queryNew, data:  [dataArray] };
}


module.exports = DbUtil;