var moment = require('moment');
var fs = require('fs');
const Store = require('electron-store');
var mySqlDateTimeFormat = 'YYYY-MM-DD HH:mm:ss';

module.exports.readImageFile = function(title,cb) {
    var dialog = require('electron').remote.dialog;
    var options = {
        title : title,
        filters: [
            { name: 'Images', extensions: ['jpg', 'png', 'gif'] }
        ],
        properties: [ 'openFile' ]
    };
    dialog.showOpenDialog( options, function(filePath){
       
        fs.readFile(filePath[0], function(err, fileData) {
               console.log(err || filePath); 
            var fileName = filePath[0].split("\\")[filePath[0].split("\\").length-1];
            return cb({imageName : fileName, imageData: fileData});
        });
        
    });
};

function formatDate(date, format) {
    if (typeof format === 'undefined' || format === null) {
        return moment(date).format(mySqlDateTimeFormat);
    } else {
        return moment(date).format(format);
    }
}

function formRequestData(data,func) {
    const store = new Store();
    
    if(func === 'insert') {
        data.created_date = formatDate(new Date(),mySqlDateTimeFormat);
        data.created_by = store.get('userId');
        data.updated_date = formatDate(new Date(),mySqlDateTimeFormat);
        data.updated_by = store.get('userId');
        return data;
    } else if(func === 'update') {
        if(data.created_date != undefined) {
            delete data.created_date;
        }
        if(data.created_by != undefined) {
            delete data.created_by;
        }
        data.updated_date = formatDate(new Date(),mySqlDateTimeFormat);
        data.updated_by = store.get('userId');
        return data;
     } else 
        return 'Function Not Found';  
     
}
function getFromLocal(property) {
    const store = new Store();
    return store.get(property);
}

function getGSTList() {
    const store = new Store();
    if(store.get('gstList') === undefined) {
        store.set('gstList', ['0%','5%','12%','18%','28%']);
    }
    return store.get('gstList');
}
module.exports.getGSTList = getGSTList;
module.exports.getFromLocal = getFromLocal;
module.exports.formatDate = formatDate;
module.exports.formRequestData = formRequestData;