var DbUtil = require('./util/dbUtil');
var sql = require('./config/sql.json');

module.exports.getPurchaseReport = function(event,cb) {
    try {
         var connection = new DbUtil();
         
         var resp;
         var queryFetchPurchaseReport = sql.report.fetchPurchaseOverAllReport;
         const companyId = event.companyId;
         connection.fetchSQLWithData(queryFetchPurchaseReport,companyId, function(err, rows) {
            if(err) throw err;
            
             if(rows.length == 0) {
                 resp = {
                     success : false,
                     message : 'No Purchase Report Found',
                     count : rows.length
                 };
             }
             else {
                 resp = {
                   success : true,
                   message : 'fetch Data Successful',
                   data : rows[0],
                   count : rows.length
               };
             }
             return cb(null, resp); 
         }); 
    } catch(ex) {
       resp = {
         success : false,
         message : 'Exeption : ' + ex
       };
       console.log(JSON.stringify(resp));    
       return cb(null, resp); 
     }
  };

