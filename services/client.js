var DbUtil = require('./util/dbUtil');
var sql = require('./config/sql.json');
var commonUtils = require('./util/common');

var mysqlDateTimeFormat = 'YYYY-MM-DD HH:mm:ss';
const Store = require('electron-store');

module.exports.getAllClients = function(cb) {
    try {
         var connection = new DbUtil();
         
         var resp;
         var queryFetchAllClients = sql.client.queryFetchClients;
         const store = new Store();
          const companyId = store.get('companyId');
        
         connection.fetchSQLWithData(queryFetchAllClients,companyId, function(err, rows) {
            if(err) throw err;
            console.log(rows);
             if(rows.length == 0) {
                 resp = {
                     success : false,
                     message : 'No Results,Click to Add the Client',
                     count : rows.length
                 };
             }
             else {
                 resp = {
                   success : true,
                   message : 'fetch Data Successful',
                   data : rows,
                   count : rows.length
               };
             }
             return cb(null, resp); 
         }); 
    } catch(ex) {
       resp = {
         success : false,
         message : 'Exeption : ' + ex
       };
       console.log(JSON.stringify(resp));    
       return cb(null, resp); 
     }
  };

  module.exports.insertClient = function(companyData, cb) {
    var response;
    try { 
    var connection = new DbUtil();
    var currentDate = new Date();
    var queryToInsertClient = sql.client.queryInsertClient;
    
    const store = new Store();
    const companyId = store.get('companyId');
    const userId = store.get('userId');
    companyData.company_id_fk = companyId;
    companyData.active_flag = 'Y';
    companyData.created_date = commonUtils.formatDate(currentDate,mysqlDateTimeFormat);
    companyData.created_by = userId;
    console.log(companyData);

    connection.executeSQL(queryToInsertClient,companyData, function(err, result){
     if(err) throw err;
        response = {
            success : true,
            message : 'Insert Success'
        };
        console.log(response);
        return cb(err, response);
    });
 } catch(ex) {
      response = {
        success : false,
        message : 'Exeption : ' + ex
      };
      console.log(JSON.stringify(response));    
      return cb(null, response);    
  }
};


module.exports.updateClient = function(clientId,clientData, cb) {
    var response;
    try { 
    var connection = new DbUtil();
    var currentDate = new Date();
    var queryToUpdateClient = sql.client.queryUpdateClient;
    
    const store = new Store();
    const companyId = store.get('companyId');
    var updateData = JSON.parse(clientData);
    updateData.updated_date = commonUtils.formatDate(currentDate,mysqlDateTimeFormat);
    updateData.updated_by = store.get('userId');
    connection.executeSQLWithValues(queryToUpdateClient,updateData,[ companyId, clientId ], function(err, result){
     if(err) throw err;
        console.log(result);
        response = {
            success : true,
            message : 'update Success'
        };
        console.log(response);
        return cb(err, response);
    });
 } catch(ex) {
      response = {
        success : false,
        message : 'Exeption : ' + ex
      };
      console.log(JSON.stringify(response));    
      return cb(null, response);    
  }
}

module.exports.deleteClient = function(clientId, cb) {
    var response;
    try { 
    var connection = new DbUtil();
    var currentDate = new Date();
    var queryToUpdateClient = sql.client.queryDeleteClientById;
    
    const store = new Store();
    
    const companyId = store.get('companyId');
    var updated_date = commonUtils.formatDate(currentDate,mysqlDateTimeFormat);
    var updated_by = store.get('userId');
    
    var executeData = [];
    executeData.push([
        'N',
        updated_by,
        updated_date,
        companyId,
        clientId
    ]);

    connection.executeSQL(queryToUpdateClient,executeData, function(err, result){
     if(err) throw err;
        console.log(result);
        response = {
            success : true,
            message : 'Delete Success'
        };
        console.log(response);
        return cb(err, response);
    });
 } catch(ex) {
      response = {
        success : false,
        message : 'Exeption : ' + ex
      };
      console.log(JSON.stringify(response));    
      return cb(null, response);    
  }
}
