var app = angular.module("myAppProduct", ['pascalprecht.translate']);
var languages = require('../../language.json');
var isInsert = false;
app.config(function($translateProvider) {
    $translateProvider.translations('en', languages.en)
    .translations('tn', languages.tn);
    $translateProvider.preferredLanguage(languages.preferedLanguage);
  });
  
app.controller("productCtrl", function($scope, $window,$translate) {
  var services = require('../../../services/product');  
  $scope.listProducts = [];
  $scope.searchText = '';

  $scope.getAllProducts = function() {
    $scope.loaderActivated = true;
    var services = require('../../../services/product'); 
    
    services.getAllProducts(function(err, res) {
      if(err) throw err;
      
      if(res.success == false && res.count == 0) {
          $scope.addProduct();
          showAlert(res.message);
          $scope.loaderActivated = false; 
          $scope.$applyAsync();
          return;
      } else if(res.success == false) {
          showAlert(res.message); 
          $scope.loaderActivated = false;
          $scope.$applyAsync();
          return;
      }
          $scope.isDataView = true;
          $scope.isEditView = false;
          $scope.output = res.data;
          $scope.listProducts = res.data;
          getGSTRateList();
          $scope.loaderActivated = false;
          $scope.$applyAsync();
          
     });   
  }
  

  $scope.addProduct = function() {
    $scope.productEditData = {
        uom : 'Kg',
        gst : '0%',
        alert_when_reach_min : 'Y'
    };
    $scope.isDataView = false;
    $scope.isEditView = true;
    isInsert = true;
    console.log('coming to add Client');
    $scope.$applyAsync();
  }

  $scope.editProduct = function(productId){
    $scope.isEditView = true;
    $scope.isDataView = false;
    isInsert = false;
     console.log(productId); 
    
     for (var c = 0; c < $scope.output.length; c++) {
       if(String($scope.output[c].product_id) == String(productId)) {
          $scope.productEditData = $scope.output[c];
       }
    }
   
 }

 $scope.complete=function(string){
    
    var output=[];
    angular.forEach($scope.output,function(item){
      if(item.product_name.toLowerCase().indexOf(string.toLowerCase())>=0) {
        output.push(item);
      }
    });
    $scope.listProducts=output;
  }

  
  $scope.saveProduct = function(isValid) {
    console.log('coming to Save Client');
  if (!isValid) {
      $window.alert('Please Fill the Required Fields');
      return;
  }
  $scope.loaderActivated = true;
  $scope.$applyAsync();
  if(isInsert === true) {
      services.insertProduct($scope.productEditData, function(err, data) {
      if(err) throw err;
       if(!data.success){  
        showAlert(data.message); 
        return
       } 
        showAlert('Product has Successfully Added','OK');          
        $scope.getAllProducts();
        
     });
  } else {
     var productData = angular.toJson($scope.productEditData);
     services.updateProduct($scope.productEditData.product_id,productData, function(err, data) {
        if(err) throw err;
        if(!data.success) {  
            alert(data.message); 
            return
         }
         showAlert('Product has Successfully Updated');
         $scope.getAllProducts(); 
     });
   }
 }

 $scope.deleteProduct = function(productId) {
    
    showWarningAlert('Are you sure?','Once deleted, you will not be able to recover..!', function(willDelete){
      if(willDelete) {
        $scope.loaderActivated = true;
        $scope.$applyAsync();
        services.deleteProduct(productId, function(err, data) {
          if(err) throw err;
          if(!data.success) {  
            showAlert(data.message); 
              return
           }
           $scope.getAllProducts(); 
       }); 
      }  
    });
    
   
 }

 function getGSTRateList() {
    var common = require('../../../services/util/common'); 
    $scope.gstList = common.getGSTList();
 }
 
 
 $scope.canceledClicked = function() {
    $scope.isEditView = false;
    $scope.isDataView = true;
 };

});