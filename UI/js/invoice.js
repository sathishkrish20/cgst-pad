
var app = angular.module("myApp", ['pascalprecht.translate','moment-picker','dx']);
var languages = require('../../language.json');


app.config(function($translateProvider) {
    $translateProvider.translations('en', languages.en)
    .translations('tn', languages.tn);
    $translateProvider.preferredLanguage(languages.preferedLanguage);
});

  app.config(['momentPickerProvider', function(momentPickerProvider) {
    momentPickerProvider.options({
        /* Picker properties */
        locale:        'en',
        minView:       'decade',
        maxView:       'day',
        startView:     'day',
        autoclose:     true,
        today:         true,
        keyboard:      true,
        leftArrow:     '&larr;',
        rightArrow:    '&rarr;',
        yearsFormat:   'YYYY',
        monthsFormat:  'MMM',
        daysFormat:    'D',
        hoursFormat:   'HH:[00]',
        minutesFormat: moment.localeData().longDateFormat('LT').replace(/[aA]/, ''),
        secondsFormat: 'ss',
        minutesStep:   5,
        secondsStep:   1
    });
}]);

  
app.controller("invoiceCtrl", function($scope, $window,$translate,$location,$timeout, $q, $log) {
  
  $scope.language = 'en';
  $scope.languages = ['en', 'tn'];
  
  $scope.updateLanguage = function() {
    $translate.use($scope.language);
  };

  var invoiceId;
  var hasSaved = false;
  var invoiceType;
 $scope.initialLoad = function()  {
    var path = $location.absUrl();
    var subPath = path.substring(0, path.indexOf('#')).split('/');
    var searchObject = $location.search('invoiceId');
    var queryParam = searchObject.hash();
    invoiceId = getParameterByName('invoiceId', queryParam);
    console.log(invoiceId);
    if(subPath[subPath.length-1] == 'invoice.view.html') { // View Mode
        getAllInvoice();
    }

    else if(subPath[subPath.length-1] == 'invoice.add.html' && (invoiceId == '' || invoiceId == null)) { // Create Mode
        $scope.invoiceProductData = [];
        $scope.clientFormData = {
         invoiceType : 'sale',     
         purchased_date : moment(new Date()).format('YYYY-MM-DD'),
         issued_date : moment(new Date()).format('YYYY-MM-DD'),
        totalAmtForAllProduct : 0,
        totalDiscountForAllProduct : 0,
        totalGSTAmountForAllProduct : 0,
        shippingCharge : 0,
        shippingTax : '0%',
        shippingChargesWithTax : 0,
        finalAllProductsAmount : 0,
        cashPaidBy : 'Cash',
        paidAmount : 0,
        isShippingChargesEnabled : false,
        saleOrPurchase : 'sale'
     };
        $scope.activeClassForSale = 'active show';
        $scope.activeClassForPurchase = '';
        $scope.editAddedProduct = -1;
        $scope.singleProductData = {};
        getGSTRateList();
        getAllClients();
        getAllProducts();
    }
    else if(subPath[subPath.length-1] == 'invoice.add.html' && queryParam != '' && (invoiceId != '' || invoiceId != null)) { // Edit Mode 
        getGSTRateList();
        hasSaved = true;
        alert(queryParam);
        $scope.getInvoiceDataById(invoiceId);   
    }
    $scope.$applyAsync();
    
    // list of `state` value/display objects
 }

 

    
  getAllInvoice = function () { 
    $scope.loaderActivated = true;
    $scope.$applyAsync();
    var services = require('../../../services/invoice'); 
    $scope.listItems= [];
    services.getAllInvoice( function(err, res){
      if(err) throw err;
        if(res.success == 'false') {
          showAlert(res.message); 
          return;
        }
        $scope.listItems = res.data;
        $scope.loaderActivated = false;
        $scope.$applyAsync();
     });  
 }

 $scope.getInvoiceDataById = function (invoiceId) { 
    var services = require('../../../services/invoice'); 
    services.getInvoiceData(invoiceId, function(err, res){
      if(err) throw err;
        if(res.success == 'false') {
          $window.alert(res.message); 
          return;
        }
        //console.log(res);
        $scope.invoiceProductData = res.data;
        //console.log($scope.invoiceProductData);
        $scope.companyData = {
            company_name : res.data[0].companyName,
            address_line1: res.data[0].companyAddressLine1,
            address_line2:res.data[0].companyAddressLine2,
            city: res.data[0].companyCity,
            state : res.data[0].companyState,
            country : res.data[0].companyCountry,
            pincode : res.data[0].companyPinCode,
            mobile_no : res.data[0].companyMobileNo,
            email : res.data[0].companyEmail     
        };
        $scope.clientFormData = {
            invoiceType : res.data[0].invoice_type,
            invoice_id : res.data[0].invoice_id,
            client_id : res.data[0].client_id,
            client_name : res.data[0].client_name,
            mobile_no : res.data[0].mobile_no,
            address_line1: res.data[0].address_line1,
            address_line2:res.data[0].address_line2,
            city: res.data[0].city,
            state : res.data[0].state,
            country : res.data[0].country,
            pincode : res.data[0].pincode,
            
            purchased_date : res.data[0].purchased_date,
            issued_date : res.data[0].issued_date,
            shippingCharge : res.data[0].shipping_charge,
            shippingTax : res.data[0].shipping_tax,
            shippingChargesWithTax :Number(res.data[0].shippingAmountWithTax),
            totalAmtForAllProduct : Number(res.data[0].products_amount),
            totalDiscountForAllProduct : Number(res.data[0].products_discount_amount),
            totalGSTAmountForAllProduct : Number(res.data[0].products_gst_amount),
            finalAllProductsAmount : Number(res.data[0].products_amount) - Number(res.data[0].products_discount_amount) + Number(res.data[0].products_gst_amount),
            cashPaidBy : res.data[0].paid_by,
            paidAmount : Number(res.data[0].paid_amount),
            isShippingChargesEnabled : (res.data[0].shipping_charge == 0) ? false : true
        } 
        $scope.calculateShippingCharge();
        $scope.$applyAsync();
     });  
 }




$scope.addProduct = function(isValid) {
    
    if(!isValid) {
        $window.alert('Please Fill the Product');
        return false;
    }
    if($scope.singleProductData.quantity > $scope.singleProductData.available_quantity) {
         showAlert('You are Exceeding the Product');
    }
    
    var totalAmt = Number($scope.singleProductData.quantity ) * Number($scope.singleProductData.price);
    var discountAmount = (Number($scope.singleProductData.discount) * totalAmt) / 100;
    var priceAfterDisc =  totalAmt - discountAmount;

    
    var gst = $scope.singleProductData.gst.replace('%','').trim();
    var GSTAmount = (Number(gst) * priceAfterDisc) / 100; 
    var totalPriceForProduct = parseFloat(priceAfterDisc + GSTAmount).toFixed(2);
    
    var data = {
           product_id : $scope.singleProductData.product_id, 
           product_name : $scope.singleProductData.product_name,
           description : $scope.singleProductData.description,
           uom : $scope.singleProductData.uom,
           quantity : $scope.singleProductData.quantity,
           price : $scope.singleProductData.price,
           discount : $scope.singleProductData.discount,
           gst :$scope.singleProductData.gst,
           priceAfterDiscountForProduct : Number(totalPriceForProduct),
           pActive : 'Y',
           totalAmount : Number(totalAmt),
           discountAmount : Number(discountAmount),
           gstAmount : Number(GSTAmount),
           available_quantity : $scope.singleProductData.available_quantity
       }; 
       
       $scope.clientFormData.totalAmtForAllProduct += data.totalAmount;
       $scope.clientFormData.totalDiscountForAllProduct += data.discountAmount;
       $scope.clientFormData.totalGSTAmountForAllProduct += Number(data.gstAmount);
       
       $scope.clientFormData.finalAllProductsAmount +=  Number(totalPriceForProduct);
       $scope.invoiceProductData.push(data);
       //calculateTotalInvoiceProductAmount();
       $scope.singleProductData = {};
}


$scope.editAddedProduc = function(index) {
    $scope.editAddedProduct = index;
    $scope.singleProductEditingData = $scope.invoiceProductData[index];
    
}

$scope.calculateAftereditedAddedProduct = function(index) {
  // Deducting the Existing value From Invoice Totals  
  
    $scope.clientFormData.totalAmtForAllProduct = Number($scope.clientFormData.totalAmtForAllProduct) - Number($scope.invoiceProductData[index].totalAmount);
    $scope.clientFormData.totalDiscountForAllProduct = $scope.clientFormData.totalDiscountForAllProduct - $scope.invoiceProductData[index].discountAmount;
    $scope.clientFormData.totalGSTAmountForAllProduct = Number($scope.clientFormData.totalGSTAmountForAllProduct) - Number($scope.invoiceProductData[index].gstAmount);
    $scope.clientFormData.finalAllProductsAmount = $scope.clientFormData.finalAllProductsAmount - $scope.invoiceProductData[index].priceAfterDiscountForProduct;
    
    
    $scope.invoiceProductData[index].gst = $scope.singleProductEditingData.gst;
    $scope.invoiceProductData[index].price = $scope.singleProductEditingData.price;
    $scope.invoiceProductData[index].quantity = $scope.singleProductEditingData.quantity;
    $scope.invoiceProductData[index].discount = $scope.singleProductEditingData.discount;
    
    

    if($scope.singleProductEditingData.quantity > $scope.invoiceProductData[index].available_quantity) {
        showAlert('You are Exceeding the Product');
    }

    var totalAmt = Number($scope.invoiceProductData[index].quantity) * Number($scope.invoiceProductData[index].price);
    var discountAmount = (Number($scope.invoiceProductData[index].discount) * totalAmt) / 100;
    var priceAfterDisc =  totalAmt - discountAmount;
    var gst = $scope.invoiceProductData[index].gst.replace('%','').trim();
    var GSTAmount = (Number(gst) * priceAfterDisc) / 100; 
    var totalPriceForProduct = parseFloat(priceAfterDisc + GSTAmount).toFixed(2);
    
    
    $scope.invoiceProductData[index].available_quantity = $scope.invoiceProductData[index].available_quantity - $scope.singleProductData.quantity

    $scope.invoiceProductData[index].priceAfterDiscountForProduct = Number(totalPriceForProduct);
    
    $scope.invoiceProductData[index].totalAmount = Number(totalAmt);
    $scope.invoiceProductData[index].discountAmount = Number(discountAmount);
    $scope.invoiceProductData[index].gstAmount = Number(GSTAmount);
    
    // adding the new calculated value for Total invoices
    
    $scope.clientFormData.totalAmtForAllProduct += Number($scope.invoiceProductData[index].totalAmount);
    
    $scope.clientFormData.totalDiscountForAllProduct += $scope.invoiceProductData[index].discountAmount;
    $scope.clientFormData.totalGSTAmountForAllProduct += Number($scope.invoiceProductData[index].gstAmount);
    
    $scope.clientFormData.finalAllProductsAmount +=  Number(totalPriceForProduct);

    $scope.editAddedProduct = -1;
}

$scope.deleteProduct = function(index) { 
    
    $scope.clientFormData.totalAmtForAllProduct = $scope.clientFormData.totalAmtForAllProduct - $scope.invoiceProductData[index].totalAmount;
    $scope.clientFormData.totalDiscountForAllProduct = $scope.clientFormData.totalDiscountForAllProduct - $scope.invoiceProductData[index].discountAmount;
    $scope.clientFormData.totalGSTAmountForAllProduct = Number($scope.clientFormData.totalGSTAmountForAllProduct) - Number($scope.invoiceProductData[index].gstAmount);
    $scope.clientFormData.finalAllProductsAmount = $scope.clientFormData.finalAllProductsAmount - $scope.invoiceProductData[index].priceAfterDiscountForProduct;
    $scope.invoiceProductData[index].pActive = 'N';   
    $scope.$applyAsync();    
}

$scope.saveInvoice = function(isValid) {
    if(!isValid) {
        $window.alert('Please Fill The Required Field');
        return false;
    }
    var services = require('../../../services/invoice');
    
    
if(hasSaved == false) {
      services.addProdtoInvoce($scope.clientFormData, $scope.invoiceProductData, function(err,data){
        if(err) throw err;
        console.log(data);
        hasSaved = true;
        invoiceId = data.invoiceId;
        $scope.showInfo(invoiceId);

    });
} else {
    console.log($scope.clientFormData);
    services.updateProdtoInvoce(invoiceId,$scope.clientFormData, $scope.invoiceProductData, function(err,data){
        if(err) throw err;
        console.log(data);
        hasSaved = true;
        invoiceId = data.invoiceId;
        $scope.showInfo(invoiceId);
    });
}
    
}



$scope.popupOptions = {
    width: 1020,
    height: 700,
    contentTemplate: "info",
    showTitle: true,
    title: "Information",    
    dragEnabled: false,
    closeOnOutsideClick: true,
    bindingOptions: {
        visible: "visiblePopup",
    }
};

$scope.showInfo = function (data) {
    $scope.getInvoiceDataById(data);   
    $scope.visiblePopup = true;
}; 

$scope.closePopup = function() {
    $scope.visiblePopup = false;
}

function getGSTRateList() {
    var common = require('../../../services/util/common'); 
    $scope.gstList = common.getGSTList();
}

$scope.clientDataList = [];
 function getAllClients() {
    
    var services = require('../../../services/client'); 
    services.getAllClients(function(err, res) {
      if(err) throw err;
      
      if(res.success == false && res.count == 0) {
          res.data = [];     
      } else if(res.success == false) {
          showAlert(res.message); 
          return;
      }
          $scope.clientDataList = res.data;
          $scope.clientDataOutput = res.data;
          
     });
}


function getAllProducts() {
    
    var services = require('../../../services/product'); 
    services.getAllProducts(function(err, res) {
      if(err) throw err;
      
      if(res.success == false && res.count == 0) {
          res.data = [];     
      } else if(res.success == false) {
          $window.alert(res.message); 
          return;
      }
          $scope.productDataList = res.data;
          $scope.productDataOutput = res.data;
     });
}
  

$scope.fillTextbox = function(searchedObj,property) {
    if(property == 'client_name') {
        $scope.clientFormData.client_id = searchedObj.client_id;
        $scope.clientFormData.client_name = searchedObj.client_name;
        $scope.clientDataList = null;
        $scope.clientAutoCompleteVisible = false;    
    } else if(property == 'product_name') {
        $scope.singleProductData = searchedObj;
        $scope.singleProductData.discount = 0;
        $scope.productDataList = null;
        $scope.productAutoCompleteVisible = false;
        
    } 
}

$scope.getFilteredList=function(string,property) {
    console.log(string);
   if(string === undefined){
      return false;
   }

$scope.clientAutoCompleteVisible = false; 
$scope.productAutoCompleteVisible = false; 
    var data = [];
   if(property === 'client_name') {
        data = $scope.clientDataOutput;
        $scope.productDataList = [];
    }
    else if(property === 'product_name') {
       data = $scope.productDataOutput;
       $scope.clientDataList = [];
    } 
  
  var output=[];
  angular.forEach(data,function(item){
    if(item[property].toLowerCase().indexOf(string.toLowerCase())>=0) {
      item.searchedText = string;
      output.push(item);
     }
  });
  
  if(property === 'client_name') {
      $scope.clientDataList = output;
      $scope.clientAutoCompleteVisible = true; 
  }
    else if(property === 'product_name') 
      $scope.productDataList = output;  
      $scope.productAutoCompleteVisible = true;
}

$scope.calculateShippingCharge = function() {
    if($scope.clientFormData.isShippingChargesEnabled) {
        var shippingCharge = $scope.clientFormData.shippingCharge;
        var shippingTax = $scope.clientFormData.shippingTax.replace('%','');
        var TaxAmount = (Number(shippingTax) * Number(shippingCharge)) / 100;
        $scope.clientFormData.shippingChargesWithTax = Number(shippingCharge) + TaxAmount;
        $scope.clientFormData.shipping_tax_amount = TaxAmount;
    } else {
        $scope.clientFormData.shippingCharge = 0;  
        $scope.clientFormData.shippingTax = '0%';
        $scope.clientFormData.shipping_tax_amount = 0;
        $scope.clientFormData.shippingChargesWithTax = 0;
    }
}

$scope.changeTab = function(tab) {
    $scope.invoiceProductData = [];
    $scope.clientFormData = {
        invoiceType : tab,
        purchased_date : moment(new Date()).format('YYYY-MM-DD'),
        issued_date : moment(new Date()).format('YYYY-MM-DD'),
        totalAmtForAllProduct : 0,
        totalDiscountForAllProduct : 0,
        totalGSTAmountForAllProduct : 0,
        shippingCharge : 0,
        shippingTax : '0%',
        shippingChargesWithTax : 0,
        finalAllProductsAmount : 0,
        cashPaidBy : 'Cash',
        paidAmount : 0,
        isShippingChargesEnabled : false,
        saleOrPurchase : tab
     };
        $scope.editAddedProduct = -1;
        $scope.singleProductData = {};

  if(tab== 'sale') {
     
     $scope.activeClassForSale = 'active show';
     $scope.activeClassForPurchase = '';
     $scope.clientForm.$setUntouched();
     $scope.productForm.$setUntouched();
  } else if(tab =='purchase') {
    $scope.activeClassForSale = '';
    $scope.activeClassForPurchase = ' active show';
    $scope.clientForm.$setUntouched();
    $scope.productForm.$setUntouched();
  }
  showAlert('You Selected ' + tab + ' Invoice');

}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
});