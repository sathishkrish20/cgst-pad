var app = angular.module("myAppCompany", ['pascalprecht.translate']);
var languages = require('../../language.json');

app.config(function($translateProvider) {
    $translateProvider.translations('en', languages.en)
    .translations('tn', languages.tn);
    $translateProvider.preferredLanguage(languages.preferedLanguage);
  });
  

app.controller("companyCtrl", function($scope, $window,$translate) {
  
  $scope.imageFileName = 'Logo';
  $scope.language = 'en';
  $scope.languages = ['en', 'tn'];
  
  $scope.updateLanguage = function() {
    $translate.use($scope.language);
  };
   
  
$scope.currentTime = function () {
    $scope.currentTime = new Date().toDateString;
 }  
$scope.companyData= {};
$scope.isCompanyDataPresent= false;
$scope.getCompany = function () {
    var services = require('../../../services/company'); 
     
    services.getCompany( function(err, res){
      if(err) throw err;
      console.log(res);
       if(res.data.length !== 0) {
          $scope.isCompanyDataPresent = true;
          $scope.companyData = res.data[0];

          $scope.$apply();
      }
    });
 }

 $scope.saveCompany = function(isValid) {

  if (!isValid) {
      $window.alert('Please Fill the Required Fields');
      return;
  }
  var services = require('../../../services/company'); 
  console.log($scope.isCompanyDataPresent);
  if($scope.isCompanyDataPresent === false) {
     services.insertCompany($scope.companyData, function(err, data) {
      if(err) throw err;
      //update the data
       $scope.getCompany();
     });
  } else {
     services.updateCompany($scope.companyData, function(err, data) {
        if(err) throw err;
        //update the data
        $scope.getCompany();
     });
   }
 }

 $scope.openDialogBox = function () {
   var common = require('../../../services/util/common'); 
   common.readImageFile('Upload Logo', function(logo){
      $scope.companyData.logo = logo.imageData;
      $scope.imageFileName = logo.imageName;
      console.log($scope.imageFileName );
      $scope.$apply();
   });
}



});


