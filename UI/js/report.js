var app = angular.module("myAppReport", ['pascalprecht.translate']);
var languages = require('../../language.json');
var services = require('../../../services/report.js');
  
app.config(function($translateProvider) {
    $translateProvider.translations('en', languages.en)
    .translations('tn', languages.tn);
    $translateProvider.preferredLanguage(languages.preferedLanguage);
  });
  

app.controller("reportCtrl", function($scope, $window,$translate) {
  
  $scope.imageFileName = 'Logo';
  $scope.language = 'en';
  $scope.languages = ['en', 'tn'];
  
  $scope.updateLanguage = function() {
    $translate.use($scope.language);
  };
   
  
$scope.currentTime = function () {
    $scope.currentTime = new Date().toDateString;
}  

$scope.initLoad = function() {
  $scope.currentTime();
  $scope.getReport();
}

$scope.getReport = function() {
  let data = {
    companyId : 23
  }
  services.getPurchaseReport(data,function(err, res) {
    if(err) throw err;
    
    if(res.success == false && res.count == 0) {
        return;
    } else if(res.success == false) {
        return;
    }   
        $scope.totalPurchaseData = res.data;
        $scope.$applyAsync();
        
   });   
}



});


