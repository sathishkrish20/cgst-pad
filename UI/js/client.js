var app = angular.module("myAppClient", ['pascalprecht.translate']);
var languages = require('../../language.json');
var isInsert = false;
app.config(function($translateProvider) {
    $translateProvider.translations('en', languages.en)
    .translations('tn', languages.tn);
    $translateProvider.preferredLanguage(languages.preferedLanguage);
  });
  
app.controller("clientCtrl", function($scope, $window,$translate) {
  
  $scope.listClients = [];
  $scope.searchText = '';

  $scope.complete=function(string){
    
    var output=[];
    angular.forEach($scope.output,function(item){
      if(item.client_name.toLowerCase().indexOf(string.toLowerCase())>=0) {
        output.push(item);
      }
    });
    $scope.listClients=output;
  }

  $scope.getAllClients = function() {
    $scope.loaderActivated = true;
    var services = require('../../../services/client'); 
    
    services.getAllClients(function(err, res) {
      if(err) throw err;
      
      if(res.success == false && res.count == 0) {
          $scope.addClient();
          $window.alert(res.message); 
          return;
      } else if(res.success == false) {
          $window.alert(res.message); 
          return;
      }
          $scope.isDataView = true;
          $scope.isEditView = false;
          $scope.output = res.data;
          $scope.listClients = res.data;
          $scope.loaderActivated = false;
          $scope.$applyAsync();
     });
  }
  
  $scope.addClient = function() {
    $scope.clientEditData = {};
    $scope.isDataView = false;
    $scope.isEditView = true;
    isInsert = true;
    console.log('coming to add Client');
    $scope.$applyAsync();
  }

  $scope.saveClient = function(isValid) {
    if (!isValid) {
        $window.alert('Please Fill the Required Fields');
        return;
    }
    $scope.loaderActivated = true;
    $scope.$applyAsync();
    var services = require('../../../services/client');
    var clientData = angular.toJson($scope.clientEditData);
    if(isInsert === true) {
        services.insertClient($scope.clientEditData, function(err, data) {
        if(err) throw err;
         if(!data.success){  
          showAlert(data.message); 
          $scope.loaderActivated = false;
          $scope.$applyAsync();
          return
         }           
          $scope.getAllClients();
          //console.log(data);
       });
    } else {
       services.updateClient($scope.clientEditData.client_id,clientData, function(err, data) {
          if(err) throw err;
          if(!data.success){
            showAlert(data.message);   
            $scope.loaderActivated = false;
            $scope.$applyAsync();
            return
           }
           $scope.getAllClients(); 
       });
     }
   }
   
   
   $scope.editClient = function(clientId){
      $scope.isEditView = true;
      $scope.isDataView = false;
      isInsert = false;
       console.log(clientId); 
      
       for (var c = 0; c< $scope.output.length; c++) {
         if(String($scope.output[c].client_id) == String(clientId)) {
            $scope.clientEditData = $scope.output[c];
         }
      }
      //console.log($scope.clientEditData);
   }

$scope.deleteClient = function(clientId) {
  showWarningAlert('Are you sure?','Once deleted, you will not be able to recover..!', function(willDelete){
     if(willDelete) {
      $scope.loaderActivated = true;
      $scope.$applyAsync();
     var services = require('../../../services/client');
     services.deleteClient(clientId, function(err, data) {
      if(err) throw err;
       if(!data.success) {  
        showAlert(data.message); 
          return
       }
       $scope.getAllClients(); 
      });
     } 
  });
}

  $scope.canceledClicked = function() {
    $scope.isEditView = false;
    $scope.isDataView = true;
  };

  $scope.openDialogBox = function () {
    $scope.isEditView = true;  
    $scope.isDataView = false;
    var common = require('../../../services/util/common'); 
    common.readImageFile('Upload Image', function(logo) {
      $scope.isEditView = true;  
      $scope.isDataView = false;
      
      $scope.clientEditData.image = logo.imageData;
      $scope.imageFileName = logo.imageName;
      console.log($scope.imageFileName );
      $scope.$apply();
    });
}

});  
