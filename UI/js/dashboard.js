
var app = angular.module("myApp", ['pascalprecht.translate']);
//var con = require('../../language.json');
var languages = {
  	"en" : {
    "en": "English",
    "tn": "தமிழ்",      
    "TITLE": "Welcome!",
    "MESSAGE": "This app supports your lanaguage!",
    "DASHBOARD" : "Dashboard",
    "INVOICES" : "invoices",
    "NEW_INVOICE" : "New Invoice",
    "COMPANY" : "Company",
    "COMPANY_DETAILS" : "Company Details",
    "COMPANY_NAME" : "Name",
    "COMPANY_NAME_PLACEHOLDER" : "Company Name",
    "EMAIL" : "Email",
    "MOBILE_NO" : "Mobile",
    "WEBSITE" : "Website",
    "ADDRESS" : "Address",
    "ADDRESS_1" : "Address 1",
    "ADDRESS_2" : "Address 2",
    "CITY" : "City",
    "STATE" : "State",
    "COUNTRY" : "Country",
    "PINCODE" : "Pin Code",
    "Upload" : "Upload",
    "Save" : "Save",
    "COMPANY_OWNER_NAME" : "Owner Name",
    "CLIENTS" : "Clients",
    "CLIENT" : "Client",
    "PRODUCTS" : "Products"
  },
  "tn": {
    "en": "English",
    "tn": "தமிழ்",      
    "TITLE": "வணக்கம்!",
    "MESSAGE": "Denna app stöder ditt språk!",
    "DASHBOARD" : "முகப்பு",
    "INVOICES" : "விலைப்பட்டியல்கள்",
    "INVOICE" : "விலைப்பட்டியல்",
    "NEW_INVOICE" : "புதிய விலைப்பட்டியல்",
    "COMPANY" : "நிறுவனம்",
    "COMPANY_DETAILS" : "நிறுவனத்தின் தகவல்கள்",
    "COMPANY_NAME" : "பெயர்",
    "COMPANY_NAME_PLACEHOLDER" : "நிறுவனத்தின் பெயர்",
    "EMAIL" : "மின்னஞ்சல்", 
    "MOBILE_NO" : "அலைபேசி",
    "WEBSITE" : "இணையம்",
    "CATEGORY" : "பிரிவு",
    "ADDRESS" : "முகவரி",
    "ADDRESS_1" : "முகவரி 1",
    "ADDRESS_2" : "முகவரி 2",
    "CITY" : "நகரம்",
    "PINCODE" : "அஞ்சல் குறியீடு",
    "STATE" : "மாநிலம்",
    "COUNTRY" : "நாடு",
    "Upload" : "Upload",
    "Save" : "பதிவேற்று",
    "COMPANY_OWNER_NAME" : "உரிமையாளர் பெயர்",
    "CLIENTS" : "வாடிக்கையாளர்கள்",
    "CLIENT" : "வாடிக்கையாளர்",
    "PRODUCTS" : "பொருட்கள்"
  }
}

app.config(function($translateProvider) {
    $translateProvider.translations('en', languages.en)
    .translations('tn', languages.tn);
    
    $translateProvider.preferredLanguage('tn');
  });
  

app.controller("dashboardCtrl", function($scope, $window,$translate) {
  
  $scope.language = 'en';
  $scope.languages = ['en', 'tn'];
  
  $scope.updateLanguage = function() {
    $translate.use($scope.language);
  };
   
  
 $scope.getAllInvoice = function () { 
    var services = require('../../../services/invoice'); 
    $scope.listItems= [];
    services.getAllInvoice( function(err, res){
      if(err) throw err;
        if(res.success == 'false') {
          $window.alert(res.message); 
          return;
        }
        $scope.listItems = res.data;
        $scope.$apply();
     });  
 }
    

    
$scope.currentTime = function () {
    $scope.firstName = "John";
    $scope.lastName = "Doe";
    //$scope.myText = "Hello";
     console.log('comign here');
     $scope.myText = 'aa.name'; 
}

$scope.invoiceProductData = [];

$scope.addProduct = function () {
    
    var totalAmt = Number($scope.pQty ) * Number($scope.pPrice);
    var discountAmount = (Number($scope.pDiscount) * totalAmt) / 100;
    var priceAfterDisc =  totalAmt - discountAmount;
    var GSTAmount = (Number($scope.pGST) * priceAfterDisc) / 100; 
    var totalPriceForProduct = parseFloat(priceAfterDisc + GSTAmount).toFixed(2);
    var data = {
           id : $scope.invoiceProductData.length +1, 
           pName : $scope.pName,
           pUOM : $scope.pUOM,
           pQty : $scope.pQty,
           pPrice : $scope.pPrice,
           pDiscount : $scope.pDiscount,
           pGST : $scope.pGST,
           pTotal : totalPriceForProduct,
           pActive : 'Y'
       }; 
       $scope.invoiceProductData.push(data);
       console.log($scope.invoiceProductData);
}


var invoiceId;
var hasSaved = false;
$scope.saveInvoice = function() {
    var services = require('../../../services/invoice');
    var companyData = {
        companyId : 1,
        createdDate : new Date().toISOString(),
        userId : 1
    };  
if(hasSaved == false) {
      services.addProdtoInvoce(companyData, $scope.invoiceProductData, function(err,data){
        if(err) throw err;
        console.log(data);
        hasSaved = true;
        invoiceId = data.invoiceId;
    });
} else {
    services.updateProdtoInvoce(invoiceId,companyData, $scope.invoiceProductData, function(err,data){
        if(err) throw err;
        console.log(data);
        hasSaved = true;
        invoiceId = data.invoiceId;
    });
}
    
}

});


/*
(function ($) {
  'use strict';
  $(function () {
    if ($('#dashboard-area-chart').length) {
      var lineChartCanvas = $("#dashboard-area-chart").get(0).getContext("2d");
      var data = {
        labels: ["2013", "2014", "2014", "2015", "2016", "2017"],
        datasets: [{
            label: 'Product',
            data: [0, 11, 6, 10, 8, 0],
            backgroundColor: 'rgba(0, 128, 207, 0.4)',
            borderWidth: 1,
            fill: true
          },
          {
            label: 'Product',
            data: [0, 7, 11, 8, 11, 0],
            backgroundColor: 'rgba(2, 178, 248, 0.4)',
            borderWidth: 1,
            fill: true
          },
          {
            label: 'Support',
            data: [0, 14, 10, 14, 6, 0],
            backgroundColor: 'rgba(73, 221, 255, 0.4)',
            borderWidth: 1,
            fill: true
          }
        ]
      };
      var options = {
        responsive: true,
        maintainAspectRatio: true,
        scales: {
          yAxes: [{
            display: false
          }],
          xAxes: [{
            display: false,
            ticks: {
              beginAtZero: true
            }
          }]
        },
        legend: {
          display: false
        },
        elements: {
          point: {
            radius: 3
          }
        },
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        stepsize: 1
      };
      var lineChart = new Chart(lineChartCanvas, {
        type: 'line',
        data: data,
        options: options
      });
    }
  });
})(jQuery);*/